express = require('express')
socketio = require('socket.io')

app = express.createServer();

var io = socketio.listen(app)
var port = process.env.PORT || 5000;

io.configure(function () { 
  io.set("transports", ["xhr-polling"]); 
  io.set("polling duration", 10); 
});

io.sockets.on("connection", function(socket){
    socket.on("newMessage", function(data){
        io.sockets.emit('sendEvent', data)
    });
});

app.listen(port)
